import java.util.Scanner;

public class FahrsimulatorAufgabe {
	
	static double beschleunigen(double v_0, double dv) {
		return v_0 + dv * 3.6;
	}
	
	static double timeToMaxVelocity(double v_initial, double dv) {
		double maxVelocity = 130.0;
		return (maxVelocity - v_initial) /(dv * 3.6);
		
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie die Beschleunigung in m/s^2: ");
		double dv = myScanner.nextDouble();
		double stopTime = timeToMaxVelocity(0.0, dv);
		double v_prev = 0.0;
		
		for(int t=1; t<stopTime; t++) {
			double v_current = beschleunigen(v_prev, dv);
			System.out.printf("Geschwindigkeit nach %d Sekunde(n): %.2f km/h \n", t, v_current);
			v_prev = v_current;
		}
		
		System.out.printf("130 km/h war nach %.3f Sekunden erreicht\n", stopTime);
		
		myScanner.close();
	}

}
