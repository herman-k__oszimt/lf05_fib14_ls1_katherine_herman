import java.util.Scanner;


public class Mathe {
	
	static double quadrat(double x) {
		return x*x;
	}
	
	static double hypothenuse(double kathete1, double kathete2) {
		double hypothenuseSquared = Math.pow(kathete1, 2.0) + Math.pow(kathete2, 2.0);
		return Math.pow(hypothenuseSquared, 0.5);
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie den Zahl zu quadrieren: ");
		double val = myScanner.nextDouble();
		
		double valQuadrat = quadrat(val);
		
		System.out.printf("\n\nDer Quadrat von %.3f ist %.5f \n", val, valQuadrat);
		
		System.out.println("Rechnet die Lange der Hypothenuse");
		System.out.print("Bitte geben Sie die Lange der erste Kathete: ");
		double side1 = myScanner.nextDouble();
		
		System.out.print("Bitte gebe Sie die Lange der zweite Kathete: ");
		double side2 = myScanner.nextDouble();
		
		System.out.printf("Die Hypothenuse is %f", hypothenuse(side1, side2));
		
		myScanner.close();
	}

}
