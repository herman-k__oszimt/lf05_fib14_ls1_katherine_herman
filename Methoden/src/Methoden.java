import java.util.Scanner;

public class Methoden {
	
	static double reihenschaltung(double x1, double x2) {
		return x1 + x2;
	}
	
	static double parallelschaltung(double x1, double x2) {
		double inverseResistance = 1/x1 + 1/x2;
		return 1/inverseResistance;
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie das erste Widerstandswert in Kilo-Ohm: ");
		double resistance_1 = myScanner.nextDouble();
		
		System.out.print("Bitte geben Sie das zweite Wiederstandswert in Kilo-Ohm: ");
		double resistance_2 = myScanner.nextDouble();
		
		System.out.printf("\n\nErsatz Widerstand für eine Reiheschaltung: %.2f %n", reihenschaltung(resistance_1, resistance_2));
		System.out.printf("Ersatz Widerstand für eine Parallelschaltung: %.2f %n", parallelschaltung(resistance_1, resistance_2));
		
		myScanner.close();
	}

}
