import java.util.Scanner;

public class Fahrsimulator {
	/*
	 * An interactive version of assignment 6 in AB 3.2 (Methoden) version 2
	 * Just for fun
	 */
	
	
	static double beschleunigen(double v_initial, double dv) {
		int timeStep = 1; // Sekunde
		// 3600 s/hr * 1 km / 1000 m = 3.6
		return v_initial + dv*timeStep * 3.6;
	}
	

	static void runSimulation(double v_0, double dv, Scanner myScanner) {
		/*
		 * As long as the current speed is between 0 and 130 km/h, display the 
		 * current speed with a 0.5 s delay. Speed is calculated in 1 s steps. If the speed is within limits, 
		 * replace v_0 with the new current velocity and calculate the velocity for the next
		 * time step. 
		 * 
		 * If current velocity > 130 km/h, ask user to brake. Ignore positive values.
		 * 
		 * If current velocit <= 0 km/h, ask user to accelerate or end the program
		 * 
		 * Note: trusts user input!
		 */
		double v_current = beschleunigen(v_0, dv);
		int time = 0;
		boolean isContinued = true;
		
		while (isContinued) {
			time ++;
			if (v_current > 0.0 && v_current <= 130.0) {
				System.out.println("Geschwindigkeit nach " + time + " Sekunde(n): " + v_current + " km/hr");
				v_0 = v_current;
				v_current = beschleunigen(v_0, dv);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			} else if (v_current <= 0.0 ) {
				System.out.println("Sie sind gestoppt. Möchten Sie weiter fahren? y/N ");
				isContinued = myScanner.next().toLowerCase().charAt(0) == 'y';
				if (isContinued) {
					System.out.print("Geben Sie bitte Gas! Neue Beschleunigung (m/s^2): ");
					dv = myScanner.nextDouble();
					if (dv > 0) {
						v_current = beschleunigen(v_current, dv);
						time = 0;
					}
				}
			} else if (v_current >= 130.0 ){
				System.out.println("Sie fahren zu schnell, bitte bremsen!");
				System.out.print("Geben Sie ein negetives Beschleunigungswerte (m/s^2): ");
				dv = myScanner.nextDouble();
				if (dv < 0) {
					v_current = beschleunigen(v_current, dv);
					time = 0;
				}
			}
		}
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Fahrsimulator!");
		System.out.println("Tempo Limit ist 130 km/h");
		System.out.println("Ctrl+C um das Program abzubrechen");
		
		System.out.print("Initialle Geschwindigkeit ist null, geben eine Beschleunigung in m/s^2 ein: ");

		double dv = myScanner.nextDouble();
		runSimulation(0.0, dv, myScanner);
		
		myScanner.close();
	}

}
