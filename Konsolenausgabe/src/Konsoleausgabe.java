
public class Konsoleausgabe {
	
	public static void printTree() {
		int treeWidth = 13;
		int halfTreeWidth = Math.floorDiv(treeWidth, 2);
		
		for(int i=0; i<halfTreeWidth; i++) {
			String spaces = "";
			String stars = "*";
			for(int j=0; j<halfTreeWidth-i; j++) {
				spaces += " ";
			}
			for (int k=0; k<i; k++) {
				stars += "**";
			}
			System.out.println(spaces + stars);
		}
		System.out.printf("%8s\n", "***");
		System.out.printf("%8s\n", "***");
	}
	
	public static void printFormatedDouble() {
		double numberArr[] = {22.4234234, 111.2222, 4.0, 1000000.551, 97.34};
		for(int i=0; i<numberArr.length; i++) {
			System.out.printf("%.2f\n", numberArr[i]);
		}
	}
	

	public static void main(String[] args) {
		//printTree();
		printFormatedDouble();
	}

}
