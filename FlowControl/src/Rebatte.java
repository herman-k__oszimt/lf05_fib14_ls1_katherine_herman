import java.util.Scanner;

public class Rebatte {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte geben Sie die Bestellwert in Euro: ");
		double bestellwert = myScanner.nextDouble();
		
		if(bestellwert <= 0 ) {
			System.out.println("invalid input/keine Rebatte");
		} else if (bestellwert > 0 && bestellwert < 100) {
			System.out.println("10 % Rebatte");
		} else if (bestellwert >= 100 && bestellwert < 500) {
			System.out.println("15 % Rebatte");
		} else {
			System.out.println("20 % Rebatte");
		}
		
		myScanner.close();

	}

}
