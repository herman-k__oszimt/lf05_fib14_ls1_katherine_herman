import java.util.Scanner;

public class Monate {

	public static void main(String[] args) {
		
		System.out.print("Für welchen Monate möchten Sie der Name habe? (Ganzzahl): ");
		int monthNumber = myScanner.nextInt();
		
		while (monthNumber < 1 || monthNumber > 12) {
			System.out.print("Invalid request. Bitte geben Sie eine Zahl zwischen 1 und 12, inklusiv: ");
			monthNumber = myScanner.nextInt();
		}
		
		System.out.println("Der " + monthNumber + ". Monate ist " + months[monthNumber-1]);
		
		myScanner.close();
	}
	
	private static String[] months = {"Januar", "Febrar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"};
	private static Scanner myScanner = new Scanner(System.in);
}
