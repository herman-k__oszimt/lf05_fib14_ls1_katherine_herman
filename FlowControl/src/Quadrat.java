import java.util.Scanner;

public class Quadrat {
	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("How many units would you like each side of the sqare to have?: ");
		int sideLength = myScanner.nextInt();
		
		// Prepare string for top and bottom rows
		String solidBar = "* ";
		for (int i=1; i<sideLength-1; i++) {
			solidBar += " * ";
		}
		solidBar += " *";
		
		// Prepare string for middle rows
		String hollowBar = "* ";
		for (int i=1; i<sideLength-1; i++) {
			hollowBar += "   ";
		}
		hollowBar += " *";
		
		
		// Yes, could have built the strings each times using nested for loops
		// and performance doesn't matter for such a small program, but still
		// don't see any reason to run in n^2 time when 3n (effectively, n) suffices
		for (int i = 0; i<sideLength; i++) {
			if (i == 0 || i == sideLength -1) {
				System.out.println(solidBar);
			} else {
				System.out.println(hollowBar);
			}
		}
		
		myScanner.close();
	}

}
