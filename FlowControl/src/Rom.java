import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Rom {
	
	/*
	 * Check if string only contains valid Roman numerals
	 * 
	 * param: inNumeral the string to check
	 * return: boolean true if valid string
	 */
	protected static boolean onlyValidCharacters(String inNumeral) {
		boolean isValid = true;
		
		for (int i=0; i < inNumeral.length(); i++) {
			int index = romanNumerals.indexOf(inNumeral.charAt(i));
			if (index == -1) {
				System.out.println("Contains invalid character(s)");
				isValid = false;
				break;
			}
		}
		
		return isValid;
	}
	
	/*
	 * Check if the string follows the rules for repetition
	 * No character should be repeated more than three times in a row
	 * 
	 * param: inNumeral the string to check
	 * return: boolean true if valid string
	 */
	protected static boolean checkRepetition(String inNumeral) {
		boolean isValid = true;
		int accumulator = 1;
		String neverRepeat = "VLD";
		char prevChar = inNumeral.charAt(0);
		
		for (int i=1; i<inNumeral.length(); i++) {
			char currentChar = inNumeral.charAt(i);
			
			if (currentChar == prevChar) {
				accumulator += 1;

				if (neverRepeat.indexOf(currentChar) != -1 || accumulator > 3) {
					System.out.println("Illegal character repetition.");
					System.out.println("Either chacter in {V, L, D} repeated, or a character is repeated more than three times in a row.");
					isValid = false;
					break;
				}
			} else {
				accumulator = 1;
				prevChar = currentChar;
			}
		}
		
		return isValid;
	}
	
	
	/*
	 * Check if the rules for subtraction are adhered to 
	 * I can only be followed by X or V
	 * X can only be followed by L or C
	 * C can only be followed by D or M
	 * Value is subtracted if following numeral has a larger value (checked using the position in the romanNumerals array).
	 * Numerals to be subtracted should  not be repeated (accumulated)
	 * 
	 * param: inNumeral the string to check
	 * return: boolean true if valid string
	 */
	protected static boolean checkSubtraction(String inNumeral) {
		boolean isValid = true;
		char prevChar = inNumeral.charAt(0);
		int accumulator = 1;
		Map<Character, String> map = new HashMap<Character, String>();
		
		map.put('I', "XV");
		map.put('X', "LC");
		map.put('C', "DM");
		
		for(int i=1; i< inNumeral.length(); i++) {
			char currentChar = inNumeral.charAt(i);
			
			if (romanNumerals.indexOf(prevChar) < romanNumerals.indexOf(currentChar)) {
				
				switch(prevChar) {
				case 'I':
				case 'X':
				case 'C':
					if (map.get(prevChar).indexOf(currentChar) == -1 || accumulator > 1) {
						System.out.println("illegal subtraction " + currentChar);
						isValid = false;
					}
					break;
				default:
					if (accumulator > 1) {
						System.out.println("illegal subtraction "+ currentChar);
						isValid = false;
					}
					break;
				}
			}
			
			if (currentChar == prevChar) {
				accumulator += 1;
			} else {
				accumulator = 1;
			}
			
			prevChar = currentChar;
		}
		
		return isValid;
	}
	

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		boolean isValid;
		
		System.out.print("Enter a Roman numeral to check: ");
		String inNumeral = myScanner.nextLine().toUpperCase();
		
		isValid = onlyValidCharacters(inNumeral);
		isValid = checkRepetition(inNumeral);
		isValid = checkSubtraction(inNumeral);
		
		if (isValid == true) {
			System.out.println("Valid Roman numeral!");
		}
		
		myScanner.close();
	}
	
	private static String romanNumerals = "IVXLCDM";

}
