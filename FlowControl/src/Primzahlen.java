
public class Primzahlen {

	public static void main(String[] args) {

		String listPrimes = "";
		
		for (int i=2; i<=100; i++) {
			
			// only need to test for factors up to the square root of the largest number
			// => only primes less than 10 need to be checked
			// used continues instead of a long string of logical and's for readability
			if ( i%2 == 0 && i/2 > 1) {
				continue;
			} else if (i%3 == 0 && i/3 > 1) {
				continue;
			} else if (i%5 == 0 && i/3 > 1) {
				continue;
			} 
			else if (i%7 == 0 && i/7 > 1) {
				continue;
			} else {
				listPrimes = listPrimes + " " + i + ",";
			}
		}
		
		System.out.println(listPrimes);

	}

}
