import java.util.Scanner;

public class sortieren_abc {
	
	static char getInput(Scanner myScanner) {
		System.out.print("Bitte geben Sie ein Charakter oder Ziffer ein: ");
		return myScanner.next().charAt(0);
	}
	
	static void simpleSort(char[] valArr) {
		// I think this is basically bubble sort? Not very efficient, but conceptually simple
		char currentChar;
		char currentMin;
		int swapIndex = -1;
		
		// iterate through the array values. 
		// don't need the last value since the array is already sorted by the time we reach it
		for (int i=0; i<valArr.length - 1; i++) {
			currentChar = valArr[i];
			currentMin = valArr[i];
			swapIndex = -1;
			
			//optional, if we want a = 1 etc..
			//int test = (int)valArr[i] - (int)'a' + 1;
			
			// iterate through the remaining array and check which value is larger
			// store current min value and its index
			for (int j=i+1; j<valArr.length; j++) {
				
				if (valArr[j] < currentMin) {
					currentMin = valArr[j];
					swapIndex = j;
				}
			}
			
			// if starting value (index i) was not the smallest, then swap values
			if(i != swapIndex && swapIndex != -1) {
				valArr[i] = currentMin;
				valArr[swapIndex] = currentChar;
			}
		}
		
		System.out.println(valArr);
	}

	public static void main(String[] args) {
		int numInputValues = 3;
		int arrayCounter = 0;
		char nextVal;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie " + numInputValues + " Charakter oder Ziffer, die sortiert werden sollen");
		
		// ArrayList would be nice, but this also works
		char[] valArr = new char[numInputValues];
		
		for(int i=0; i<numInputValues; i++) {
			arrayCounter += 1;
			nextVal = getInput(myScanner);
			valArr[arrayCounter-1] = nextVal;
		}
		
		simpleSort(valArr);
		
		myScanner.close();
		
		/*
		char[] testArr = {'a', '8', '2'};
		
		simpleSort(testArr);
		*/

	}

}
