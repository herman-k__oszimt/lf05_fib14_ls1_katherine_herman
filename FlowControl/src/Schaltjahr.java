import java.util.Scanner;

public class Schaltjahr {
	
	static void checkYear(int jahr) {
		if ( jahr%4==0 && (jahr%400==0 || jahr%100 != 0)) {
			System.out.printf("%d war ein Schaltjahr", jahr);
		} else {
			System.out.printf("%d war kein Schaltjahr", jahr);
		}
	}

	public static void main(String[] args) {
		
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Bitte ein Jahr eingeben > ");
		int jahr = myScanner.nextInt();
		
		checkYear(jahr);
		myScanner.close();
		
		/*
		int[] testYears = {1601, 1700, 1701, 1600,1972, 1720};
		String[] testResults = {"false", "false", "false", "true", "true", "true"};
		for(int i=0; i<testYears.length; i++) {
			checkYear(testYears[i]);
			System.out.println(" " + testResults[i] + "\n");
		}
		*/
		

	}

}
