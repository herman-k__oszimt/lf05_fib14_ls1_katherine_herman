import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class RomTest extends Rom {
	
	@Test
	void testOnlyValidCharacters() {

		assertEquals(Rom.onlyValidCharacters("IVXLCDM"), true);
		assertEquals(Rom.onlyValidCharacters("I"), true);
		assertEquals(Rom.onlyValidCharacters("IVXLCDM3"), false);
		assertEquals(Rom.onlyValidCharacters("ABCD"), false);
		assertEquals(Rom.onlyValidCharacters("VIIZ"), false);
	}
	
	
	
	@Test
	void testCheckRepetition() {
		
		assertEquals(Rom.checkRepetition("III"), true);
		assertEquals(Rom.checkRepetition("VIII"), true);
		assertEquals(Rom.checkRepetition("IIII"), false);
		assertEquals(Rom.checkRepetition("LL"), false);
		assertEquals(Rom.checkRepetition("DD"), false);
		assertEquals(Rom.checkRepetition("LVII"), true);
		assertEquals(Rom.checkSubtraction("MMMCMXCIX"), true);
	}
	
	
	
	@Test
	void testCheckSubtraction() {
		
		assertEquals(Rom.checkSubtraction("IV"), true);
		assertEquals(Rom.checkSubtraction("IX"), true);
		assertEquals(Rom.checkSubtraction("IM"), false);
		assertEquals(Rom.checkSubtraction("IIV"), false);
		assertEquals(Rom.checkSubtraction("CMIV"), true);
		assertEquals(Rom.checkSubtraction("CMIIV"), false);
		assertEquals(Rom.checkSubtraction("MMMCMXCIX"), true);
	}

}
