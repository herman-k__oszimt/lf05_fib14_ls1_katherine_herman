import java.util.Scanner;

public class Konsoleneingabe {
	

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Simple calculator. Supports: ");
		System.out.println("\t'+' addition \n\t'-' subtraction \n\t'*' multiplication \n\t'/' division \n\n");
		
		
		boolean isValidOperation = false;
		char opSymbol = ' ';
		
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		
		int zahl1 = myScanner.nextInt();
		
		while (!isValidOperation) {
			System.out.println("Please enter the symbol for operation you wish to perform: ");
			opSymbol = myScanner.next().charAt(0);
			if(opSymbol == '+' || opSymbol == '-' || opSymbol == '*' || opSymbol == '/') {
				isValidOperation = true;
			}
		}
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		
		int zahl2 = myScanner.nextInt();
		
		int ergebnis = 0;
		int remainder = 0;
		
		switch(opSymbol) {
		case '+':
			ergebnis = zahl1 + zahl2;
			break;
		case '-':
			ergebnis = zahl1 - zahl2;
			break;
		case '*':
			ergebnis = zahl1 * zahl2;
			break;
		case '/':
			ergebnis = zahl1 / zahl2;
			remainder += zahl1 % zahl2;
			break;
		default: 
			System.out.println("Error: Invalid operation.");
		}
		
		System.out.print("\n\nErgebnis lautet: ");
		System.out.printf("%d %s %d = %d", zahl1, opSymbol, zahl2, ergebnis);
		
		if (remainder != 0) {
			System.out.printf(" remainder: %d", remainder);
		}

		myScanner.close();
	}

}
