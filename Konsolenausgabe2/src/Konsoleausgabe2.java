
public class Konsoleausgabe2 {
	
	public static void printStar(String side) {
		// generate stars for printShape
		String str = "*";
		
		if (side == "left") {
			System.out.printf("%-4s", str);
		} else if (side == "right") {
			System.out.printf("%4s", str);
		} else {
			System.out.print("\n");
		}
	}
	
	public static void printShape() {
		String[] directions = {"right", "left", "left", "right", "left", "right", "right", "left"}; 
		for(int i=0; i<directions.length; i++) {
			printStar(directions[i]);
			if(i%2 == 1) {
				System.out.print("\n");
			}
		}
	}
	
	public static void formattedFactorials() {
		int maxInput = 5;
		int factorialVal = 1;
		String factorialStr = " ";
		
		for(int i=0; i<=maxInput; i++) {
			
			if (i > 1) {
				factorialStr += " * ";
			}
			
			if (i != 0) {
				factorialVal *= i;
				factorialStr += String.valueOf(i);
			}
			
			System.out.printf("%d", i);
			System.out.printf("%-4s","!");
			System.out.printf("=%-20s", factorialStr);
			System.out.print("=");
			System.out.printf("%4d\n", factorialVal);
		}
	}

	public static void main(String[] args) {
		// make table header
		String horizontalSeparator = "";
		for(int i=0; i<24; i++) {
			horizontalSeparator += "-";
		}
		horizontalSeparator += "\n";
		
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s\n", "Celsius");
		System.out.print(horizontalSeparator);
		
		// calculate temperature values
		for(int i=-20; i<=30; i+=10) {
			String fahrenheitStr =  String.valueOf(i);
			if(i>=0) {
				fahrenheitStr = "+" + fahrenheitStr;
			} 
			
			double celsiusVal = (i - 31) * ((float)5/9);
			
			// format temperature values
			System.out.printf("%-12s", fahrenheitStr);
			System.out.print("|");
			System.out.printf("%10.2f\n", celsiusVal);
		}
	}

}
