public class ConfigurationsTest {
	
	public static void exercise1() {
		// declaration
		// int cent; 
		// declaration with initialization
		int cent = 80;
		cent = 70;
		
		float maximum = 95.50f;
		System.out.printf("Exercise 1:\n\t int: %d, double: %f \n", cent, maximum);
	}
	
	public static void exercise2() {
		boolean verbrauchen = true;
		short smallInt = -1000;
		float smallDec =  1.255f;
		char poundSymbol = '#';
		System.out.printf("Exercise 2:\n\t boolean: %b, short: %d, float: %f, char: %s \n", verbrauchen, smallInt, smallDec, poundSymbol);
	}
	
	public static void exercise3() {
		String sentence = "mit drei Wörter";
		final int CHECK_NR = 8765;
		System.out.printf("Exercise 3:\n\t sentence: %s, final int: %d \n", sentence, CHECK_NR);
	}
	
	public static void exercise4() {
		int ergebnis = 4 + 8 * 9 - 1;
		int zaehler = 1;
		zaehler++;
		
		int ganzzahldivision = 22/6;
		
		System.out.printf("Exercise 4:\n\t zahler: %d, ergebnis: %d, ganzzahldivision: %d\n", zaehler, ergebnis, ganzzahldivision);
	}
	
	public static void exercise5() {
		System.out.println("Exercise 5: ");
		int schalter = 10;
		if(schalter > 7 && schalter < 12) {
			System.out.println("\t schalter is in range 7 to 12");
		} else {
			System.out.println("\t is out of range");
		}
		
		if(schalter != 10 || schalter == 12) {
			System.out.println("\t not equal 10 or equal 12");
		} else {
			System.out.println("\t equal 10 or not equal 12");
		}
	}
	
	public static void exercise6() {
		System.out.print("Exercise 6:\n ");
		String part1 = "Meine Oma ";
		String part2 = "fährt im ";
		String part3 = "Hühnerstall Motorrad.";
		System.out.println("\t " + part1 + part2 + part3);
	}
	
	

	public static void main(String[] args) {
		exercise1();
		exercise2();
		exercise3();
		exercise4();
		exercise5();
		exercise6();
	}

}
