﻿import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

class Fahrkartenautomat
{
	
	static void warte(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/*
	 * Ticket class
	 * Includes toString method to print the ticket information nicely
	 * 
	 * param (String) name
	 * param (double) price
	 */
	private static class Ticket {
		String name;
		double price;
		
		public String toString() {
			return this.name + " [" + germanNumberFormatStr(this.price) + "]";
		}
	}
	
	
	/*
	 * Method to create objects of type Ticket
	 * 
	 * param (String) name: ticket name
	 * param (double) price: ticket price
	 */
	private static Ticket createTicket(String name, double price) {
		Ticket t = new Ticket();
		t.name = name;
		t.price = price;
		return t;
	}
	
	/*
	 * Method to initialize essentially a dictionary of tickets
	 * 
	 * Update here if new ticket types added
	 * 
	 * Integer field - the value the user will enter to get this ticket type
	 * Ticket field - the corresponding ticket instance
	 * Tickets are created directly by calling the createTicket method
	 * 
	 * return hashmap of Ticket instances
	 */
	private static Map<Integer, Ticket> initTicketMap() {
		Map<Integer, Ticket> ticketMap = new HashMap<Integer, Ticket>();
		ticketMap.put(1, createTicket("Einzelfahrschein Regeltarif AB", 2.90));
		ticketMap.put(2, createTicket("Tageskarte Regeltarif AB", 8.60));
		ticketMap.put(3, createTicket("Kleingruppen-Tageskarte Regeltarif AB", 23.50));
		
		return ticketMap;
	}
	
	/*
	 * Nicely print prices in Euros with German delimiter
	 * 
	 * param (double) price
	 * return (String) the price as a formatted string
	 */
	private static String germanNumberFormatStr(double value) {
		String toPrint = "" + Math.round(value * 100)/100.0;
		String cents = "";
		
		String[] splitString = toPrint.split("\\.");

		if (splitString.length < 1 ) {
			cents = "00";
		} else if (splitString[1].length() == 1) {
			cents = splitString[1] + "0";
		} else {
			cents = splitString[1];
		}
		
		
		return splitString[0] +"," + cents + " E";
	}
	
	
	/*
	 * use scanner to get number of tickets
	 * check that the number of tickets requested is allowed, fallback to 1 ticket
	 * multiply by the price per ticket
	 * 
	 * return: price to pay
	 */
	private static double fahrkartenbestellungErfassen(Ticket ticket, double balance) {
		int numTickets = -1;
		boolean firstRequest = true;
		
		while (numTickets < 1 || numTickets > 10) {
			if(firstRequest == false) {
				System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
			}
			System.out.print("Anzahl Fahrkarten: ");
			numTickets = myScanner.nextInt();
			firstRequest = false;
		}
		

		balance += numTickets * ticket.price;

	    System.out.printf("Zu zahlender Betrag (EURO): %.2f \n", balance);
	       
	    return balance;
	}
	
	
	/*
	 * request coins until order price met or exceeded
	 * 
	 * return: change to return
	 */
	private static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfeneMünze;
		double rückgabebetrag;
		double eingezahlterGesamtbetrag = 0.0;
		String toPrint;
		
	    while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	  System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	  eingeworfeneMünze = myScanner.nextDouble();
	          eingezahlterGesamtbetrag += eingeworfeneMünze;
	          toPrint = germanNumberFormatStr(eingezahlterGesamtbetrag) + " von " + germanNumberFormatStr(zuZahlenderBetrag) + " bezahlt\n";
	          toPrint += "Noch zu zahlen:" + germanNumberFormatStr(zuZahlenderBetrag - eingezahlterGesamtbetrag) + "\n";
	    	  System.out.print(toPrint);
	       }
	    
	       System.out.println("\n\n");
	       
	       rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
	       
	       return rückgabebetrag ;
       }
	
	
	/*
	 * Display to indicate tickets printing
	 */
	static void fahrkartenAusgeben() {
	       System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          warte(250);
	       }
	}
	

	
	
	/*
	 * Determine coins to be given as change
	 * 
	 * param: rückgabebetrag - amount overpaid
	 */
	static void rueckgeldAusgeben(double rückgabebetrag) {
		if(rückgabebetrag > 0.0) {
			System.out.printf("\nDer Rückgabebetrag in Höhe von %.2f EURO \n", rückgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");
			
			int amount = (int)(rückgabebetrag*100);
	   		int[] coins = {200, 100, 50, 20, 10, 5};

			for(int coin : coins) {
				warte(200);
				while(amount >= coin) {
					amount -= coin;
					if (coin < 100) {
						System.out.print(coin + " cent\n");
					} else {
						System.out.print(Math.round(coin/100.0) + " Euro\n");
					}
				}
			}
	           
		}
	}
	
	
    public static void main(String[] args)
    {
    	Map<Integer, Ticket> ticketMap = initTicketMap();
    	
    	String greeting = "Fahrkartenbestellvorgang:\n" + 
    			"=========================\n\n"+
    			"Wählen Sie ihre Wunschfahrkarte für Berlin AB aus\n";
 
    	for (int i=1; i<= ticketMap.size() ; i++) {
    		greeting = greeting + ticketMap.get(i) +" (" + i + ")\n";
		}
    	greeting += "\nIhre Wahl: ";
    	
    	boolean showGreeting = true;
    	int ticketKey;
    	double zuZahlenderBetrag = 0.0;
    	
    	// can exit the loop by giving a non-integer input (e.g. 'q')
    	while (true) {
    		if(showGreeting == true) {
    			System.out.print(greeting);
    		}
    		
    		
    		try {
    			ticketKey = myScanner.nextInt();
    		} catch (InputMismatchException e){
    			break;
    		}
    		
    		Ticket ticket = ticketMap.get(ticketKey);
    		
    		if(ticket == null) {
    			System.out.print(" >>falsche Eingabe<<\nIhre Wahl: ");
    			showGreeting = false;
    			continue;
    		}
    		
    		// tracks balance, but not ticket type ^^
    		zuZahlenderBetrag = fahrkartenbestellungErfassen(ticket, zuZahlenderBetrag);
    		
    		System.out.print("Would you like to buy additional tickets? (y/N): ");
    		boolean isContinueOrder = myScanner.next().toLowerCase().charAt(0) == 'y';
    		if(isContinueOrder == true) continue;
    		
    	       
	       // Geldeinwurf
	       // -----------
	       
	       double rueckgeld = fahrkartenBezahlen(zuZahlenderBetrag);
	       
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben();
	       
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       rueckgeldAusgeben(rueckgeld);

	       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
	                          "vor Fahrtantritt entwerten zu lassen!\n"+
	                          "Wir wünschen Ihnen eine gute Fahrt.\n\n\n");
	       
	       // Clean up
	       zuZahlenderBetrag = 0.0;
	       showGreeting = true;
	       warte(1000);
    	}
       
    	/*
    	 * pretty 'shutdown' message if user entered non-numeric char
    	 */
    	System.out.print("Shutting down");
    	for(int i=0; i<3; i++) {
    		warte(500);
    		System.out.print(".");
    	}
    	myScanner.close();
    	System.out.println("\nOFF");
    }
    
    private static Scanner myScanner = new Scanner(System.in);

}