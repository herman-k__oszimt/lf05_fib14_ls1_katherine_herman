import java.util.Scanner;

public class SimpleMatrix {
	
	
	private void print2DMatrix(int[][] matrix) {
		String toPrint = "";
		for(int i=0; i<matrix.length; i++) {
			toPrint+= "| ";
			for(int j=0; j< matrix[0].length; j++) {
				toPrint = toPrint + matrix[i][j]+ "  ";
			}
			toPrint += "|\n";
		}
		System.out.print(toPrint);
	}
	
	
	
	private int[] tokenizer(String rowStr) {
		String[] rowStrArr = rowStr.split(",");
		int[] rowIntArr = new int[rowStrArr.length];
		
		for(int i=0; i< rowIntArr.length; i++) {
			try {
				rowIntArr[i] = Integer.parseInt(rowStrArr[i].trim());
			} catch (NumberFormatException e) {
				rowIntArr[i] = 0;
			}
			
		}
		return rowIntArr;
	}
	
	
	public int[][] readInMatrix(int n, int m) {
		int row = 0;
		int[][] matrix = new int[n][m];
		
		System.out.println("Non-integer characters will be replaced with 0");
		while(row < n) {
			System.out.print("Please give row "+ row + " as a comma separated list: ");
			String rowStr = myScanner.nextLine();

			int[] rowValues = tokenizer(rowStr); 
			if(rowValues.length == m) {
				matrix[row] = rowValues;
				row++;
			} else {
				System.out.println("Invalid row. Please ensure the list has "+ m+ " values");
			}
				
		}
		
		return matrix;
	}
	
	
	public boolean isSymmetricMatrix(int[][] matrix) {
		// first check that matrix is square
		if (matrix.length != matrix[0].length) return false;
		
		// only need to compare triangular sub-matricies, don't need the diagonal
		int offset = 1;
		for(int i=0; i<matrix.length; i++) {
			for(int j=offset; j<matrix.length; j++) {
				if(matrix[i][j] != matrix[j][i]) return false;
			}
			offset += 1;
		}
		return true;
	}
	
	
	private boolean testIsSymmetricMatrix() {
		int passed = 1;
		int[][] matrix1 = {{1, 2}, {2, 1}}; //true
		int[][] matrix2 = {{1, 2}, {2, 3}}; //true
		int[][] matrix3 = {{1, 2},{3, 4}}; //false
		int[][] matrix4 = {{1, 2, 3}, {4, 5, 6}}; //false
		int[][] matrix5 = {{1, 2, 3},{2, 10, 4},{3, 4, 100}}; //true
		int[][] matrix6 = {{1, 2, 3},{2, 10, 5},{3, 4, 100}}; //false
		int[][] matrix7 = {{1, -2, 3},{-2, 10, -4},{3, -4, 100}}; //true
		
		passed = isSymmetricMatrix(matrix1) == true ? passed : passed*0;
		passed = isSymmetricMatrix(matrix2) == true ? passed : passed*0;
		passed = isSymmetricMatrix(matrix3) == false ? passed : passed*0;
		passed = isSymmetricMatrix(matrix4) == false ? passed : passed*0;
		passed = isSymmetricMatrix(matrix5) == true ? passed : passed*0;
		passed = isSymmetricMatrix(matrix6) == false ? passed : passed*0;
		passed = isSymmetricMatrix(matrix7) == true ? passed : passed*0;
		
		return passed == 1;
	}
	
	
	public void run() {
		String testResult = testIsSymmetricMatrix() == true ? "passed" : "failed";
		System.out.println("Tests for isSymmetricMatrix "+ testResult);
		
		System.out.print("How many rows does your matrix have? ");
		int n = myScanner.nextInt();
		System.out.print("How many columns does your matrix have? ");
		int m = myScanner.nextInt();
		
		// TIL: nextInt() does not consume the trailing \n
		// need to consume this character before the row values can be read in
		myScanner.nextLine();
		
		int[][] matrix = readInMatrix(n, m);
		print2DMatrix(matrix);
		
		String symmetryStr = isSymmetricMatrix(matrix) ? "" : "not ";
		System.out.println("Your matrix is " + symmetryStr + "symmetric");
		
	}
	

	public static void main(String[] args) {
		
		new SimpleMatrix().run();
		
		myScanner.close();
	}
	
	
	private static Scanner myScanner = new Scanner(System.in);

}
