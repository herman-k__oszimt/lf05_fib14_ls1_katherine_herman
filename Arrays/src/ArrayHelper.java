
public class ArrayHelper {
	
	// Aufgabe 1
	public static String convertArrayToString(int[] zahlen) {
		String str;
		try {
			str = zahlen[0] + "";
		} catch(ArrayIndexOutOfBoundsException e ) {
			str = "";
		}
		
		for(int i=1; i < zahlen.length; i++) {
			str = str + ", " + zahlen[i];
		}
		return str;
	}
	
	
	// Aufgabe 2
	public static int[] reverseArray(int[] arr) {
		int temp;
		int swapIndex;
		for(int i = 0; i < arr.length/2; i++) {
			temp = arr[i];
			swapIndex = arr.length - (i+1);
			arr[i] = arr[swapIndex];
			arr[swapIndex] = temp;
		}
		return arr;
	}
	
	// Aufgabe 3
	public static int[] newReversedArray(int[] arr) {
		int[] newArr = new int [arr.length];
		for(int i=0; i < arr.length; i++) {
			newArr[arr.length - (i+1)] = arr[i];
		}
		
		return newArr;
	}
	
	
	// Aufgabe 4
	private static double[][] makeTemperatureTable(int size) {
		double[][] tempTable = new double[size][2];
		for(int i=0; i<size; i++) {
			tempTable[i][0] = i*10.0;
			tempTable[i][1] = 5/9.0 * (tempTable[i][0] - 32); 
		}
		return tempTable;
	}
	
	private static void testFormatter(String methodName, int[][] tests) {
		System.out.println("Running tests for " + methodName + "...");
		for(int i=0; i < tests.length; i++) {
			System.out.printf("\tOutput: %s\n", methodSwitch(methodName, tests[i]));
		}
	}
	
	private static String methodSwitch(String methodName, int[] test) {
		if(methodName.equals("convertArrayToString")) {
			return convertArrayToString(test);
		} else if(methodName.equals("reverseArray")) {
			return convertArrayToString(reverseArray(test));
		} else if(methodName.equals("newReversedArray")){
			return convertArrayToString(newReversedArray(test));
		} else {
			return "unknown method";
		}
	}

	public static void main(String[] args) {
		
		System.out.print("Test array values: \n[1, 2, 3, 4, 5, 6]\n[]\n[-10, -9, -8, -7, -6]\n");
		
		// test Aufgabe 1 convertArrayToString
		int[][] test1 = {
			{1, 2, 3, 4, 5, 6},
			{},
			{-10, -9, -8, -7, -6}
		};
		testFormatter("convertArrayToString", test1);
		
		// test Aufgabe 2 reverseArray
		int[][] test2 = {
			{1, 2, 3, 4, 5, 6},
			{},
			{-10, -9, -8, -7, -6}
		};
		testFormatter("reverseArray", test2);
		
		// test Aufgabe 3 newReversedArray
		// could just reuse test2, but looks a bit confusing since test2 has
		// now been changed (reverseArray does not return a new array object)
		testFormatter("newReversedArray", test1);
		
		
		// Aufgabe 4
		double[][] tempTable = makeTemperatureTable(10);
		System.out.printf("\n\n\t%-7s |   C\n\t----------------\n", "F");
		for(int i = 0; i < tempTable.length; i++) {
			System.out.printf("\t%-7.2f | %7.2f \n", tempTable[i][0], tempTable[i][1]);
		}

	}

}
