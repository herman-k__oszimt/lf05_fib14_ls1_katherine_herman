import java.util.ArrayList;
import java.util.Arrays;

public class Lotto {
	// Einfache Übungen zu Arrays
	// Aufgabe 4
	
	// I know, we only really need to cover int[], but this is such
	// a nice chance to use generics!
	// Method that sticks to the course material follows
	private static <T> void prettyPrinter(ArrayList<T> arr) {
		String prettyString = "[ ";
		for(int i=0; i< arr.size(); i++) {
			prettyString = prettyString + arr.get(i) + " ";
		}
		prettyString += "]";
		System.out.println(prettyString);
	}
	
	// method overloading
	private static void prettyPrinter(int[] intArr) {
		String prettyString = "[ ";
		for(int i=0; i< intArr.length; i++) {
			prettyString = prettyString + intArr[i] + " ";
		}
		prettyString += "]";
		System.out.println(prettyString);
	}
	
	private static int getIndex(int[] arr, int val) {
		for(int i=0; i< arr.length; i++) {
			if (arr[i] == val) return i;
		}
		return -1;
	}

	public static void main(String[] args) {
		
		// I suppose exercise as intended
		int[] lottoNumbersArr = {3, 7, 12, 18, 37, 42};
		prettyPrinter(lottoNumbersArr);
		int[] valuesToCheck = {12, 13};
		
		for(int i: valuesToCheck) {
			if (getIndex(lottoNumbersArr, i) < 0) {
				System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten.\n", i);
			} else {
				System.out.printf("Die Zahl %d ist in der Ziehung enthalten.\n", i);
			}
		}
		
		
		// Version using generics
		System.out.print("\n\nUsing Generics:\n");
		ArrayList<Integer> lottoNumbersList = new ArrayList<Integer>(Arrays.asList(3, 7, 12, 18, 37, 42));
		prettyPrinter(lottoNumbersList);	
		for(int i: valuesToCheck) {
			if (lottoNumbersList.contains(i)) {
				System.out.printf("Die Zahl %d ist in der Ziehung enthalten.\n", i);
			} else {
				System.out.printf("Die Zahl %d ist nicht in der Ziehung enthalten.\n", i);
			}
		}
	}

}
