
public class UngeradeZahlen {
	// Einfache Übungen zu Arrays
	// Aufgabe 2

	public static void main(String[] args) {
		
		int[] ungeradeZahlen = new int[10];
		int counter = 1;
		
		for(int i = 0; i < ungeradeZahlen.length; i++) {
			ungeradeZahlen[i] = counter;
			counter += 2;
		}
		
		for(int i = 0; i < ungeradeZahlen.length; i++) {
			System.out.println(ungeradeZahlen[i]);
		}

	}

}
