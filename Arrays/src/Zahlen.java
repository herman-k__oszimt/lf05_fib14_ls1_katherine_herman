
public class Zahlen {
	// Einfache Übungen zu Arrays
	// Aufgabe 1

	public static void main(String[] args) {
		int[] zahlen = new int[10];
		
		for (int i = 0; i < zahlen.length; i++) {
			zahlen[i] = i;
		}
		
		for (int i = 0; i < zahlen.length; i++) {
			System.out.println(zahlen[i]);
		}

	}

}
