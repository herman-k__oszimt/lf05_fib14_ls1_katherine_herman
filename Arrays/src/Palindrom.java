import java.util.Scanner;

public class Palindrom {
	// Einfache Übungen zu Arrays
	// Aufgabe 3
	
	private static String reverseString(String strIn) {
		char[] lettersArr = new char[strIn.length()];
		
		for(int i=0; i< strIn.length(); i++) {
			lettersArr[i] = strIn.charAt(strIn.length() - 1 - i);
		}
		
		String reversedStr = new String(lettersArr);
		
		return reversedStr;
	}
	
	private static boolean checkPalindrome(String originalStr) {
		String reversedStr = reverseString(originalStr);
		return originalStr.equals(reversedStr);
	}
	
	private static boolean test() {
		int passed = 1;
		passed = (checkPalindrome("racecar") == true) ? passed : passed*0;
		passed = (checkPalindrome("aba") == true) ? passed : passed*0;
		passed = (checkPalindrome("abcde") == false) ? passed : passed*0;
		
		return passed==1;
	}

	public static void main(String[] args) {
		
		if(test()==true) {
			// eclipse doesn't respect ANSI escape sequences :(
			System.out.println("tests passed");
		} else {
			System.out.println("tests failed, check code");
		}
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Please give a string to reverse: ");
		String wordInStr = myScanner.next().toLowerCase();
		
		System.out.println(reverseString(wordInStr));

		if(checkPalindrome(wordInStr) == true) {
			System.out.printf("%s is a palindrome\n", wordInStr);
		} else {
			System.out.printf("%s is not a palindrome\n", wordInStr);
		}
		
		myScanner.close();
	}
	

}
